package b

class Shape(var a: Int, var b: Int) {
    val square get() = a * b
}

fun main(args: Array<String>) {
    val shape = Shape(2, 2)
    println(shape.square)
    shape.a = 20
    shape.b = 30
    println(shape.square)
}