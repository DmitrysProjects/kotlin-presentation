package d

fun main(args: Array<String>) {
    val myList = getList()
            .dropLast(1)
            .filter { it%2 == 0 }
            .map { it.toString() }
    print(myList)
}

private fun getList() = listOf<Int>(1, 2, 3, 4, 5, 6)