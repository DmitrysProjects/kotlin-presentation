package c

class MyClass {
    companion object {
        val myField = "content"
    }
}

object OtherClass {
    val myField = "otherField"
}